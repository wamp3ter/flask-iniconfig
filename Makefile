.PHONY: test clean distclean pypi DEFAULT

INSTALL_DIR=_install

DEFAULT: test

test:
	@tox

clean:
	@find . -iname "*.pyc" | grep -v _install | xargs rm -vf
	@find . -iname "*.egg*" | grep -v _install | xargs rm -rvf

distclean: clean
	@find . -iname "build" -iname "dist" | xargs rm -rvf
	@rm -rf _install
	@git clean -dfx

pypi:
	@python setup.py sdist upload -r pypi
